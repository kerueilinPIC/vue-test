import axios from 'axios';
// Add a request interceptor
const apiClient = axios.create();
apiClient.interceptors.request.use(
  function (config) {
    const date = new Date();
    // Do something before request is sent
    // console.log(config);
    config.data.date = date.toString();
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
apiClient.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  },
);
export default apiClient;
