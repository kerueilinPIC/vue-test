import { ref, reactive, onMounted } from 'vue';
import type { DataTableSortState, DataTableFilterState } from 'naive-ui';
import { NDataTable } from 'naive-ui';
import {
  TableBaseColumn,
  TableColumns,
} from 'naive-ui/es/data-table/src/interface';
interface DataItem {
  column1: number;
  column2: number;
  column3: string;
}

let column1: TableBaseColumn = {
  title: 'column1',
  key: 'column1',
  sorter: true,
  sortOrder: false,
};

let column2: TableBaseColumn = {
  title: 'column2',
  key: 'column2',
  filter: true,
  filterOptionValues: [] as number[],
  filterOptions: [
    {
      label: 'Value1',
      value: 1,
    },
    {
      label: 'Value2',
      value: 2,
    },
  ],
};

const columns = reactive<TableColumns>([
  column1,
  column2,
  {
    title: 'Column3',
    key: 'column3',
  },
]);

const dataAll: DataItem[] = Array.from({ length: 987 }, (_, index) => ({
  column1: index,
  column2: (index % 2) + 1,
  column3: 'a' + index,
}));

interface QueryResult {
  pageCount: number;
  dataAll: DataItem[];
  total: number;
}

async function query(
  page: number,
  pageSize = 10,
  order: 'ascend' | 'descend' | false = 'ascend',
  filterValues: number[] = [],
): Promise<QueryResult> {
  return new Promise((resolve) => {
    const copiedData = dataAll.map((v) => v);
    const orderedData = order === 'descend' ? copiedData.reverse() : copiedData;
    const filteredData = filterValues.length
      ? orderedData.filter((row) => filterValues.includes(row.column2))
      : orderedData;
    const pagedData = filteredData.slice(
      (page - 1) * pageSize,
      page * pageSize,
    );
    const total = filteredData.length;
    const pageCount = Math.ceil(total / pageSize);
    setTimeout(
      () =>
        resolve({
          pageCount,
          dataAll: pagedData,
          total,
        }),
      1500,
    );
  });
}

const data = ref<DataItem[]>([]);
const loading = ref(true);

const pagination = reactive({
  page: 1,
  pageSize: 10,
  pageCount: 1,
  itemCount: 0,
  //   prefix({ itemCount }: { itemCount: number }) {
  //     return `Total is ${itemCount}.`;
  //   },
});
onMounted(async () => {
  const dataResult = await query(
    pagination.page,
    pagination.pageSize,
    column1.sortOrder as 'ascend' | 'descend',
    column2.filterOptionValues as number[],
  );
  data.value = dataResult.dataAll;
  pagination.pageCount = dataResult.pageCount;
  pagination.itemCount = dataResult.total;
  loading.value = false;
});

function rowKey(rowData: DataItem) {
  return rowData.column1;
}

async function handleSorterChange(sorter: DataTableSortState) {
  if (!sorter || sorter.columnKey === 'column1') {
    if (!loading.value) {
      loading.value = true;
      const dataResult = await query(
        pagination.page,
        pagination.pageSize,
        !sorter ? false : sorter.order,
        column2.filterOptionValues as number[],
      );
      //   column1.sortOrder = !sorter ? false : sorter.order;
      data.value = dataResult.dataAll;
      pagination.pageCount = dataResult.pageCount;
      pagination.itemCount = dataResult.total;
      loading.value = false;
    }
  }
}

async function handleFiltersChange(filters: DataTableFilterState) {
  if (!loading.value) {
    loading.value = true;
    const filterValues = (filters.column2 || []) as number[];
    const dataResult = await query(
      pagination.page,
      pagination.pageSize,
      column1.sortOrder as 'ascend' | 'descend',
      filterValues,
    );
    column2.filterOptionValues = filterValues;
    data.value = dataResult.dataAll;
    pagination.pageCount = dataResult.pageCount;
    pagination.itemCount = dataResult.total;
    loading.value = false;
  }
}

async function handlePageChange(currentPage: number) {
  if (!loading.value) {
    loading.value = true;
    const dataResult = await query(
      currentPage,
      pagination.pageSize,
      column1.sortOrder as 'ascend' | 'descend',
      column2.filterOptionValues as number[],
    );
    data.value = dataResult.dataAll;
    pagination.page = currentPage;
    pagination.pageCount = dataResult.pageCount;
    pagination.itemCount = dataResult.total;
    loading.value = false;
  }
}
